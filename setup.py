#!/usr/bin/env python

# setup.py --- Setup script for calculate-server

# Copyright 2008-2010 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.


import sys
from distutils.core import setup


setup(
    name = 'calculate-lib',
    version = "2.1.10",
    description = "The library for Calculate 2",
    author = "Mir Calculate Ltd.",
    author_email = "support@calculate.ru",
    url = "http://calculate-linux.org",
    license = "http://www.apache.org/licenses/LICENSE-2.0",
    package_dir = {'calculate-lib': "."},
    packages = ['calculate-lib.pym'],
    data_files = [("/usr/share/calculate-2.0/i18n",['i18n/cl_lib_ru.mo']),
                  ("/var/calculate/remote",[])],
)
