#-*- coding: utf-8 -*-

# Copyright 2008-2010 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

#Допустимые ключи значений
#   mode -     режим переменной r-не переназначается из командной строки,
#              w-переназначается из командной строки
#   type     - тип переменной состоит из двух элементов(что это и для чего
#              это)
#   value    - значение переменной

class Data:

    # имя компьютера
    os_net_hostname = {'mode':"w"}
    # разрешенные сети
    os_net_allow ={}
    # ip на всех интерфейсах
    os_net_ip ={}

    #короткое название системы (CLD)
    os_linux_shortname={}

    #домен
    os_net_domain = {'mode':"w"}

    # Пути к ini файлам
    cl_env_path = {'value':['/var/calculate/remote/calculate.env',
                            '/var/calculate/calculate.env',
                            '/etc/calculate/calculate.env',
                            '/var/lib/calculate/calculate.env']}

    # локаль (прим: ru_RU.UTF-8)
    os_locale_locale = {}
    # язык (прим: ru_RU)
    os_locale_lang = {}
    # язык (прим: ru)
    os_locale_language = {}

    # раскладка клавиатуры для X
    os_locale_xkb = {}

    # названия используемых раскладок клавиатуры для X
    os_locale_xkbname = {}

    # архитектура компьютера (i686,x86_64)
    os_arch_machine = {}

    #проход при наложении профилей 1,2,3,4,5 и.т д
    cl_pass_step = {'mode':"w"}

    # обрабатываемый файл профиля
    cl_pass_file = {'mode':"w"}

    # корневой раздел файловой системы
    os_root_dev = {}

    # тип носителя (ram, hdd, usb-hdd, livecd)
    os_root_type = {}

    # полное название системы
    os_linux_name = {}

    # постфикс к названию системы
    os_linux_subname = {}

    # название виртуальной машины (virtualbox, vmware, qemu)
    hr_virtual = {}

    # версия системы
    os_linux_ver = {}

    # Тип профиля
    cl_pass_type = {'mode':"w"}

    # Действие программы 
    cl_pass_run = {'mode':"w"}
    
    #Логин пользователя
    ur_login = {'mode':"w"}
